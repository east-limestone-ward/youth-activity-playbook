# 2021 - Youth Super Activity

## What we did
* Car wash
* Youth help pay. Every youth needed to pay $50.
* Divided different parts of the activity among the youth.

## Notes
### Things that went well
* Got an inactive youth to come.
* Everyone was safe.
### Things that we learned
* Tent camping on concrete requires creativity.
* Prepare even earlier.
* Handle food better.
* Prepare for parking problems.
    Find possible problems beforehand.
* We need to improve logistics preparation.

### Things that we would change
* Have a theme for the devotionals.
* Have a testimony meeting.
* Learn about the campground and possible campsites beforehand.
    * Prepare for possible issues.

## Testimonies
* I testify that the Lord was watching over us, and had a part in the execution of the activity. I know that without the combined faith of everyone in the group, the activity would not have gone as well as it had. Even though it may not have gone exactly as planned, we still had fun. - Wyatt Jackson.
