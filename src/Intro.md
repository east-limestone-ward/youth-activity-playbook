# Intro
This book was created to log the activities that we have done. This book was started after the 2021 Super Activity<sup>1</sup>
to log the things we learned on that activity. We hope that this will be useful for everyone who reads this book, and that 
you will learn from our mistakes and successes.

---

<sup>1</sup> Was essentially a High Adventure organized and carried out by each ward, rather then as a Stake due to the 2019 Corona Virus.
